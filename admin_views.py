import os
import cgi

from google.appengine.api import mail
import jinja2
import webapp2
import time

import models


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

# Serves main admin page, including a post() method to send admin messages to subscribers.
class AdminMain(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('admin/admin.html')
        self.response.write(template.render())

    def post(self):
        subject = cgi.escape(self.request.get("subject"))
        admin_message = cgi.escape(self.request.get("message"))
        qry = models.Subscriber.query()
        for q in qry:
            mail.send_mail("acp2142@gmail.com", q.email, subject, admin_message)
        self.redirect("/admin")

class AdminUsers(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('admin/users.html')
        users = models.User.query().order(models.User.last_name)
        self.response.write(template.render(users = users))

class AdminDeleteUser(webapp2.RequestHandler):
    def post(self):
        user_id = cgi.escape(self.request.get("delete_button"))
        int_id = int(user_id)
        user = models.User.get_by_id(int_id)
        user.key.delete()
        time.sleep(0.1)
        self.redirect("/admin/users")

# Serves the page that lists subscribers and gives various actions to apply to them.
class AdminSubscribers(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('admin/subscribers.html')
        subscribers = models.Subscriber.query().order(models.Subscriber.email)
        self.response.write(template.render(subscribers = subscribers))


# Handler for post requests to delete a subscriber.
class AdminDeleteSubscriber(webapp2.RequestHandler):
    def post(self):
        subscriber_id = cgi.escape(self.request.get("delete_button"))
        int_id = int(subscriber_id)
        subscriber = models.Subscriber.get_by_id(int_id)
        subscriber.key.delete()
        time.sleep(0.1)
        self.redirect("/admin/subscribers")

# Handler that deals with messaging individual subscribers.
class AdminMessageSubscriber(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('admin/message.html')
        user_email = cgi.escape(self.request.get("user_email"))
        self.response.write(template.render(user_email=user_email))

    def post(self):
        email = cgi.escape(self.request.get("email"))
        subject = cgi.escape(self.request.get("subject"))
        message = cgi.escape(self.request.get("message"))
        mail.send_mail("acp2142@gmail.com", email, subject, message)
        self.redirect("/admin/subscribers")


class AdminMessageUser(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('admin/message_user.html')
        user_email = cgi.escape(self.request.get("user_email"))
        self.response.write(template.render(user_email=user_email))

    def post(self):
        email = cgi.escape(self.request.get("email"))
        subject = cgi.escape(self.request.get("subject"))
        message = cgi.escape(self.request.get("message"))
        mail.send_mail("acp2142@gmail.com", email, subject, message)
        self.redirect("/admin/users")
