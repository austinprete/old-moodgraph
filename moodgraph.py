import os
import cgi
import logging

from google.appengine.api import mail
import jinja2
import webapp2

import models
import admin_views
import beta_views
import random


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class BaseHandler(webapp2.RequestHandler):
    def handle_exception(self, exception, debug):
        # Log the error.
        logging.exception(exception)

        # Set a custom message.
        template = JINJA_ENVIRONMENT.get_template('/error.html')

        # If the exception is a HTTPException, use its error code.
        # Otherwise use a generic 500 error code.
        if isinstance(exception, webapp2.HTTPException):
            self.response.set_status(exception.code)
            self.response.write(template.render(error_code=exception.code, error_message="An HTTP error occurred."))
        else:
            self.response.set_status(500)
            self.response.write(template.render(error_code="500", error_message="An internal server error occurred."))



# Serves the main page located at "/"
class MainPage(BaseHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('index.html')
        rand_int = random.randint(1,6)
        string_int = str(rand_int)
        self.response.write(template.render(random_pic=string_int))


# Handles new subscriptions for the mailing list then redirects to the success or failure page
class Subscribe(BaseHandler):
    def post(self):
        entered_email = cgi.escape(self.request.get("user_email"))
        qry = models.Subscriber.query(models.Subscriber.email == entered_email)
        if (qry.get() != None):
            self.redirect("/sorry")
        else:
            subscriber = models.Subscriber()
            subscriber.email = entered_email
            subscriber.put()
            mail.send_mail("acp2142@gmail.com", entered_email, "MoodGraph Subscription Confirmation",
                           "Thank you for subscribing"
                           " to MoodGraph's mailing list. We will notify you with updates.")
            self.redirect("/success")


# Handler to serve subscription success page
class Success(BaseHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('success.html')
        self.response.write(template.render())


# Handler to serve subscription failure page
class Sorry(BaseHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('sorry.html')
        self.response.write(template.render())


application = webapp2.WSGIApplication([
                                          ('/', MainPage),
                                          ('/subscribe', Subscribe),
                                          ('/success', Success),
                                          ('/sorry', Sorry),
                                          # Admin views
                                          ('/admin', admin_views.AdminMain),
                                          ('/admin/subscribers', admin_views.AdminSubscribers),
                                          ('/admin/subscribers/delete', admin_views.AdminDeleteSubscriber),
                                          ('/admin/subscribers/message', admin_views.AdminMessageSubscriber),
                                          ('/admin/users', admin_views.AdminUsers),
                                          ('/admin/users/delete', admin_views.AdminDeleteUser),
                                          ('/admin/users/message', admin_views.AdminMessageUser),
                                          # Beta views
                                          ('/beta', beta_views.BetaMain),
                                          ('/beta/login', beta_views.BetaLogin),
                                          ('/beta/home', beta_views.BetaHome),
                                          ('/beta/logout', beta_views.BetaLogout),
                                          ('/beta/about', beta_views.BetaAbout),
                                          ('/beta/user', beta_views.BetaProfile),
                                          ('/beta/newuser', beta_views.BetaNewUser),
                                          ('/beta/journal', beta_views.BetaJournal),
                                          ('/beta/journal/delete', beta_views.BetaDeleteEntry),
                                          ('/beta/graphs', beta_views.BetaGraphs),
                                          ('/beta/support', beta_views.BetaSupport)]
                                      , debug=True)