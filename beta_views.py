import os
import cgi
from datetime import datetime
import time
import logging

from google.appengine.api import users, mail
from google.appengine.ext import ndb
import jinja2
import webapp2

import models


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class BaseHandler(webapp2.RequestHandler):
    def handle_exception(self, exception, debug):
        # Log the error.
        logging.exception(exception)

        # Set a custom message.
        template = JINJA_ENVIRONMENT.get_template('/beta/error.html')

        # If the exception is a HTTPException, use its error code.
        # Otherwise use a generic 500 error code.
        if isinstance(exception, webapp2.HTTPException):
            self.response.set_status(exception.code)
            self.response.write(template.render(error_code=exception.code, error_message="An HTTP error occurred."))
        else:
            self.response.set_status(500)
            self.response.write(template.render(error_code="500", error_message="An internal server error occurred."))


# Main handler for the beta site
class BetaMain(BaseHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('beta/index.html')
        user = users.get_current_user()
        if user:
            self.redirect("/beta/home")
        else:
            self.response.write(template.render())


class BetaLogin(BaseHandler):
    def get(self):
        user = users.get_current_user()
        if user:
            qry = models.User.query(models.User.email == user.email())
            if (qry.get() != None):
                self.redirect("/beta/home")
            else:
                self.redirect("/beta/newuser")
        else:
            self.redirect(users.create_login_url(self.request.uri))


class BetaLogout(BaseHandler):
    def get(self):
        self.redirect(users.create_logout_url("/beta"))


class BetaHome(BaseHandler):
    def get(self):
        google_user = users.get_current_user()
        if (google_user == None):
            self.redirect('/beta')
        else:
            template = JINJA_ENVIRONMENT.get_template('/beta/home.html')
            time.sleep(.1)
            user_qry = models.User.query(models.User.email == google_user.email())
            user = user_qry.get()
            if (user != None):
                self.response.write(template.render(name=user.first_name))
            else:
                self.redirect('/beta/newuser')


class BetaAbout(BaseHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('/beta/about.html')
        self.response.write((template.render()))


class BetaProfile(BaseHandler):
    def get(self):
        google_user = users.get_current_user()
        if (google_user == None):
            self.redirect('/beta')
        else:
            template = JINJA_ENVIRONMENT.get_template('/beta/user.html')
            user_qry = models.User.query(models.User.email == google_user.email())
            user = user_qry.get()
            self.response.write(
                template.render(name=user.first_name, full_name=(user.first_name + " " + user.last_name),
                                email=user.email,
                                gender=user.gender, age=user.age, birthday=user.birthdate))


class BetaNewUser(BaseHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('/beta/newuser.html')
        self.response.write(template.render())

    def post(self):
        email = users.get_current_user().email()
        first_name = cgi.escape(self.request.get("first_name"))
        last_name = cgi.escape(self.request.get("last_name"))
        gender = cgi.escape(self.request.get("gender"))
        birthdate = datetime.strptime(cgi.escape(self.request.get("birthdate")), '%m/%d/%Y')
        age = int((datetime.today() - birthdate).days / 365)
        user = models.User(email=email, first_name=first_name, last_name=last_name, gender=gender, birthdate=birthdate,
                           age=age)
        user.put()
        mail.send_mail("acp2142@gmail.com", email, "MoodGraph Signup Confirmation", "Hello " + first_name + ",\n\n"
                                                                                                            "Thank you for signing up for the "
                                                                                                            "MoodGraph beta site!\n\n-- The MoodGraph Team")
        mail.send_mail("acp2142@gmail.com", "acp2142@gmail.com", "MoodGraph - New User",
                       first_name + " has signed up for the MoodGraph beta site.")
        self.redirect("/beta/home")


class BetaJournal(BaseHandler):
    def get(self):
        user = users.get_current_user()
        if (user == None):
            self.redirect('/beta')
        else:
            template = JINJA_ENVIRONMENT.get_template('/beta/journal.html')
            google_user = users.get_current_user()
            user_qry = models.User.query(models.User.email == google_user.email())
            user = user_qry.get()
            user_key = ndb.Key("User", user.key.id())
            entry_qry = models.JournalEntry.query(ancestor=user_key).order(-models.JournalEntry.datetime).fetch(9)
            self.response.write(template.render(name=user.first_name, entries=entry_qry))


    def post(self):
        google_user = users.get_current_user()
        user_qry = models.User.query(models.User.email == google_user.email())
        user = user_qry.get()
        mood = cgi.escape(self.request.get("mood"))
        if (mood == ""):
            mood = "Not specified"
        if (mood != "Neutral"):
            mood_value = int(cgi.escape(self.request.get("slider_value")))
        else:
            mood_value = 0
        safe_value = cgi.escape(self.request.get("safe"))
        safe = True
        if (safe_value == ""):
            safe = False
        description = cgi.escape(self.request.get("description"))
        entry = models.JournalEntry(
            parent=ndb.Key("User", user.key.id()), mood=mood, moodValue=mood_value, safe=safe, description=description)
        entry.put()
        time.sleep(.1)
        self.redirect("/beta/journal")


class BetaDeleteEntry(BaseHandler):
    def post(self):
        google_user = users.get_current_user()
        user_qry = models.User.query(models.User.email == google_user.email())
        user = user_qry.get()
        entry_id = cgi.escape(self.request.get("delete_button"))
        int_id = int(entry_id)
        entry = models.JournalEntry.get_by_id(int_id, parent=user.key)
        entry.key.delete()
        time.sleep(.1)
        self.redirect('/beta/journal')


class BetaGraphs(BaseHandler):
    def get(self):
        user = users.get_current_user()
        if (user == None):
            self.redirect('/beta')
        else:
            template = JINJA_ENVIRONMENT.get_template("/beta/graphs.html")
            google_user = users.get_current_user()
            user_qry = models.User.query(models.User.email == google_user.email())
            user = user_qry.get()
            user_key = ndb.Key("User", user.key.id())
            entry_qry = models.JournalEntry.query(ancestor=user_key).order(models.JournalEntry.datetime)
            if entry_qry.count() >= 7:
                entry_qry1 = entry_qry.fetch(7, offset=(entry_qry.count() - 7))
            else:
                entry_qry1 = entry_qry.fetch(7)
            ratings = []
            dates = []
            ratings2 = []
            dates2 = []
            date_placeholder = None
            ratings_adder = 0
            counter = 1
            entry_counter = 1
            for entry in entry_qry1:
                if (moodIsPositive(entry.mood)):
                    value = entry.moodValue
                else:
                    value = -(entry.moodValue)
                entry_date = entry.datetime.strftime("%x")
                ratings.append(str(value))
                dates.append(entry_date)
            entry_qry2 = models.JournalEntry.query(ancestor=user_key).order(models.JournalEntry.datetime)
            for entry in entry_qry2:
                if (moodIsPositive(entry.mood)):
                    moodvalue = entry.moodValue
                else:
                    moodvalue = -(entry.moodValue)
                if (entry_counter == entry_qry2.count()):
                    if (date_placeholder):
                        if (entry.datetime.strftime('%x') != date_placeholder.strftime('%x')):
                            dates2.append(date_placeholder.strftime("%x"))
                            rating_average = ratings_adder / float(counter)
                            ratings2.append(str(round(rating_average, 1)))
                            dates2.append(entry.datetime.strftime("%x"))
                            rating_average = moodvalue
                            ratings2.append(str(rating_average))
                        else:
                            dates2.append(entry.datetime.strftime("%x"))
                            ratings_adder += moodvalue
                            counter += 1
                            rating_average = ratings_adder / float(counter)
                            ratings2.append(str(round(rating_average, 1)))
                    else:
                        dates2.append(entry.datetime.strftime("%x"))
                        ratings_adder += moodvalue
                        rating_average = ratings_adder / float(counter)
                        ratings2.append(str(round(rating_average, 1)))
                elif (date_placeholder == None):
                    ratings_adder += moodvalue
                    date_placeholder = entry.datetime
                elif (entry.datetime.strftime('%x') == date_placeholder.strftime('%x')):
                    ratings_adder += moodvalue
                    counter += 1
                else:
                    dates2.append(date_placeholder.strftime("%x"))
                    rating_average = ratings_adder / float(counter)
                    ratings2.append(str(round(rating_average, 1)))
                    ratings_adder = moodvalue
                    counter = 1
                    date_placeholder = entry.datetime
                entry_counter += 1
            self.response.write(
                template.render(name=user.first_name, dates=dates, ratings=ratings, dates2=dates2, ratings2=ratings2))


class BetaSupport(webapp2.RequestHandler):
    def get(self):
        google_user = users.get_current_user()
        if (google_user == None):
            self.redirect('/beta')
        else:
            template = JINJA_ENVIRONMENT.get_template('/beta/support.html')
            user_qry = models.User.query(models.User.email == google_user.email())
            user = user_qry.get()
            if (user != None):
                user_qry = ndb.Key("User", user.key.id())
                support_qry = models.Support.query(ancestor=user_qry).fetch()
                self.response.write(template.render(name=user.first_name,supports=support_qry))
            else:
                self.redirect('/beta/newuser')

    def post(self):
        google_user = users.get_current_user()
        user_qry = models.User.query(models.User.email == google_user.email())
        user = user_qry.get()
        relationship = cgi.escape(self.request.get("relationship"))
        name = cgi.escape(self.request.get("name"))
        email = cgi.escape(self.request.get("email"))
        phone = cgi.escape(self.request.get("phone"))
        support = models.Support(parent=ndb.Key("User", user.key.id()), relationship=relationship, name=name,
                                 email=email, phone_number=phone)
        support.put()
        time.sleep(.1)
        self.redirect("/beta/support")


def moodIsPositive(mood):
    if (mood == "Happy" or mood == "Hopeful" or mood == "Optimistic" or mood == "Peaceful"):
        return True
    else:
        return False