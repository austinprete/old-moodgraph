from google.appengine.ext import ndb

# Models a database entry for an email subscriber.
class Subscriber(ndb.Model):
    email = ndb.StringProperty()

class User(ndb.Model):
    email = ndb.StringProperty()
    first_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    gender = ndb.StringProperty()
    birthdate = ndb.DateProperty()
    age = ndb.IntegerProperty()
    phone = ndb.StringProperty(default=None)
    disorder = ndb.StringProperty(default=None)

class JournalEntry(ndb.Model):
    moodValue = ndb.IntegerProperty(default=0)
    mood = ndb.StringProperty()
    safe = ndb.BooleanProperty()
    description = ndb.StringProperty()
    datetime = ndb.DateTimeProperty(auto_now_add=True)

class Support(ndb.Model):
    relationship = ndb.StringProperty()
    name = ndb.StringProperty()
    phone_number = ndb.StringProperty()
    email = ndb.StringProperty()